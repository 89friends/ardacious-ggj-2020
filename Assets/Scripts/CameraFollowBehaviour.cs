﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowBehaviour : MonoBehaviour
{
    public GameObject targetToFollow;
    public Camera myCamera;

    private float m_currDistToTarget;

    [SerializeField]
    private float m_maxDistAllowed = 5;

    [SerializeField]
    private float m_cameraSpeed = 10;

    // Update is called once per frame
    private void Update()
    {
        FollowTarget();
    }

    private void FollowTarget()
    {
        if (targetToFollow == null)
            return;

        m_currDistToTarget = Vector3.Distance(transform.position, targetToFollow.transform.position);

        transform.LookAt(targetToFollow.transform);

        if (m_currDistToTarget > m_maxDistAllowed)
        {
            //transform.position = Vector3.Lerp(transform.position, targetToFollow.transform.position, Time.deltaTime * m_cameraSpeed);
            transform.position = Vector3.MoveTowards(transform.position, targetToFollow.transform.position, Time.deltaTime * m_cameraSpeed);
        }
    }
}
