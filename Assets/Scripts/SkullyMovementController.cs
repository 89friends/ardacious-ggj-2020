﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullyMovementController : MonoBehaviour
{
    public Rigidbody currentRigidBody;

    public Camera mainCamera;

    private float m_jumpProgress = 0;
    private float m_characterHeight;
    private Vector3 m_defaultGravity;

    [SerializeField]
    private float m_speedModifier = 10;
    [SerializeField]
    private float m_armRunSpeed = 2;
    [SerializeField]
    private float m_runSpeed = 3;
    [SerializeField]
    private float m_climbSpeed = 10;
    [Tooltip("Jump rate is the time taken in seconds to reach the maximum jump height")]
    private float m_jumpRate = 1;
    [SerializeField, Tooltip("Fall rate is a relative value in relation to the climb rate for jumps")]
    private float m_fallRate = 2;
    [SerializeField, Tooltip("Jump Multiplier of 1 jumps to the height of the character")]
    private int m_jumpMultiplier = 2;
    [SerializeField, Tooltip("How much to jump the skull when it rights itself")]
    public float skullOrientationBounce = 2f;

    [SerializeField]
    private Collider m_currentCollider;

    public enum MovementState
    {
        m_stationary,
        m_rolling,
        m_armRunning,
        m_running,
        m_climbing,
        m_jumping
    }

    private enum JumpState
    {
        j_grounded,
        j_rising,
        j_falling
    }

    public enum BodyState
    {
        b_skull,
        b_skullChest,
        b_skullChestLegs,
        b_none
    }

    private MovementState m_movementState;
    public MovementState currentMovementMode;
    private JumpState m_jumpState;
    public BodyState m_bodyState = BodyState.b_none;

    public SkullyAnimController animController;

    public GameObject skullGameObject, armsTorsoGameObject, fullBodyGameObject;
    public GameObject currentActiveObject;

    public CameraController camControl;

    // Start is called before the first frame update
    private void Awake()
    {
        m_currentCollider = skullGameObject.GetComponent<Collider>();
        currentRigidBody = skullGameObject.GetComponent<Rigidbody>();
        m_movementState = MovementState.m_stationary;
        currentMovementMode = MovementState.m_rolling;
        m_jumpState = JumpState.j_grounded;
        m_defaultGravity = Physics.gravity;
        m_characterHeight = m_currentCollider.bounds.size.y;
    }

    private void Start()
    {
        ChangeActiveSkeletonMode(BodyState.b_skull);
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        ProcessInput();

        if (Input.GetKeyDown(KeyCode.Space))
            TriggerJump();

        if (m_jumpState == JumpState.j_grounded && m_movementState != MovementState.m_climbing && !Physics.Raycast(m_currentCollider.bounds.center, Vector3.down, m_currentCollider.bounds.extents.y + 0.1f))
        {
            m_jumpState = JumpState.j_falling;
            StartCoroutine(JumpTransition());
        }
    }

    private void ProcessInput()
    {
        if (currentRigidBody == null)
            return;

        float horizontalDir = Input.GetAxis("Horizontal");
        float verticalDir = Input.GetAxis("Vertical");

        if (horizontalDir == 0 && verticalDir == 0)
        {
            animController?.SetPlayerMoving(false);
            if (m_movementState == MovementState.m_climbing)
            {
                switch (m_bodyState)
                {
                    case BodyState.b_skull:
                        break;
                    case BodyState.b_skullChest:
                        animController?.ChangeAnimState(AnimControllerState.e_ArmClimbIdle);
                        break;
                    case BodyState.b_skullChestLegs:
                        animController?.ChangeAnimState(AnimControllerState.e_FullClimbIdle);
                        break;
                    default:
                        break;
                }

            }
            else if (m_movementState != MovementState.m_stationary && m_movementState != MovementState.m_climbing && currentRigidBody.velocity.magnitude < .75f)
                ChangeMovementState(MovementState.m_stationary);
            else
                return;
        }

        else
        {
            ChangeMovementState((m_movementState == MovementState.m_climbing) ? MovementState.m_climbing : currentMovementMode);
            animController?.SetPlayerMoving(true);
        }

        Vector3 dir = Vector3.zero;

        if (m_movementState != MovementState.m_climbing)
        {
            dir = verticalDir * mainCamera.transform.forward + horizontalDir * mainCamera.transform.right;
            dir.y = 0;
        }
        else
        {
            dir = verticalDir * Vector3.up;
        }

        if (dir.sqrMagnitude > 1f)
        {
            dir.Normalize();
        }

        ProcessMovement(dir);
    }

    public void ChangeMovementState(MovementState _newMovementState)
    {
        if (m_movementState == _newMovementState)
            return;

        m_movementState = _newMovementState;

        switch (m_movementState)
        {
            case MovementState.m_stationary:
                switch (m_bodyState)
                {
                    case BodyState.b_skull:
                        animController?.ChangeAnimState(AnimControllerState.e_HeadIdle);
                        break;
                    case BodyState.b_skullChest:
                        animController?.ChangeAnimState(AnimControllerState.e_ArmIdle);
                        break;
                    case BodyState.b_skullChestLegs:
                        animController?.ChangeAnimState(AnimControllerState.e_FullIdle);
                        break;
                }
                break;
            case MovementState.m_rolling:
                animController?.ChangeAnimState(AnimControllerState.e_HeadMovement);
                break;
            case MovementState.m_armRunning:
                animController?.ChangeAnimState(AnimControllerState.e_ArmMovement);
                break;
            case MovementState.m_running:
                animController?.ChangeAnimState(AnimControllerState.e_FullMovement);
                break;
            case MovementState.m_climbing:
                switch (m_bodyState)
                {
                    case BodyState.b_skull:
                        break;
                    case BodyState.b_skullChest:
                        animController?.ChangeAnimState(AnimControllerState.e_ArmClimbMovement);
                        break;
                    case BodyState.b_skullChestLegs:
                        animController?.ChangeAnimState(AnimControllerState.e_FullClimbMovement);
                        break;
                    default:
                        break;
                }
                break;
            case MovementState.m_jumping:
                break;
            default:
                break;
        }
    }

    private void ProcessMovement (Vector3 _dir)
    {
        switch(m_movementState)
        {
            case MovementState.m_stationary:
                if (m_bodyState == BodyState.b_skull)
                {
                    currentRigidBody.velocity = Vector3.zero;
                    currentRigidBody.angularVelocity = Vector3.zero;
                    currentRigidBody.AddForce(Vector3.up * skullOrientationBounce, ForceMode.VelocityChange);
                }

                currentActiveObject.transform.rotation = Quaternion.LookRotation(mainCamera.transform.forward, Vector3.up);
                currentActiveObject.transform.eulerAngles = new Vector3(0, currentActiveObject.transform.eulerAngles.y, 0);
                currentRigidBody.useGravity = true;
                break;
            case MovementState.m_rolling:
                currentRigidBody.AddForce(_dir * m_speedModifier, ForceMode.Force);
                currentRigidBody.useGravity = true;
                break;
            case MovementState.m_armRunning:
                currentRigidBody.MovePosition(armsTorsoGameObject.transform.position + _dir * m_armRunSpeed * Time.deltaTime);
                armsTorsoGameObject.transform.rotation = Quaternion.LookRotation(mainCamera.transform.forward, Vector3.up);
                armsTorsoGameObject.transform.eulerAngles = new Vector3(0, armsTorsoGameObject.transform.eulerAngles.y, 0);
                currentRigidBody.useGravity = true;
                break;
            case MovementState.m_running:
                currentRigidBody.MovePosition(fullBodyGameObject.transform.position + _dir * m_runSpeed * Time.deltaTime);
                fullBodyGameObject.transform.rotation = Quaternion.LookRotation(mainCamera.transform.forward, Vector3.up);
                fullBodyGameObject.transform.eulerAngles = new Vector3(0, fullBodyGameObject.transform.eulerAngles.y, 0);
                currentRigidBody.useGravity = true;
                break;
            case MovementState.m_climbing:
                currentRigidBody.useGravity = false;
                currentRigidBody.MovePosition(currentActiveObject.transform.position + _dir * m_climbSpeed * Time.deltaTime);
                if (m_bodyState == BodyState.b_skullChestLegs)
                    animController?.ChangeAnimState(AnimControllerState.e_FullClimbMovement);
                else if (m_bodyState == BodyState.b_skullChest)
                    animController?.ChangeAnimState(AnimControllerState.e_ArmClimbMovement);
                break;
        }
    }

    private void ProcessJump()
    {
        switch(m_jumpState)
        {
            case JumpState.j_grounded:
                Physics.gravity = m_defaultGravity;
                m_jumpProgress = 0;
                animController?.ChangeAnimState(AnimControllerState.e_Landing);
                break;
            case JumpState.j_rising:
                Physics.gravity = m_defaultGravity / m_jumpRate;
                if (m_jumpProgress >= 1)
                    m_jumpState = JumpState.j_falling;
                animController?.ChangeAnimState(AnimControllerState.e_RisingInAir);

                break;
            case JumpState.j_falling:
                if (m_movementState != MovementState.m_climbing)
                {
                    Physics.gravity = m_defaultGravity * m_jumpRate * m_fallRate;

                    if (Physics.Raycast(m_currentCollider.bounds.center, Vector3.down, m_currentCollider.bounds.extents.y + 0.1f))
                    {
                        m_jumpState = JumpState.j_grounded;
                    }

                    if (m_bodyState == BodyState.b_skullChestLegs)
                        animController?.ChangeAnimState(AnimControllerState.e_FullFallingInAir);
                    else if (m_bodyState == BodyState.b_skullChest)
                        animController?.ChangeAnimState(AnimControllerState.e_HalfFallingInAir);
                }
                else
                    m_jumpState = JumpState.j_grounded;
                break;
        }
    }

    private void TriggerJump()
    {
        //Check Grounded State
        if (m_jumpState == JumpState.j_grounded && m_bodyState == BodyState.b_skullChestLegs)
        {
            //Distance travelled will be the average velocity over the time period of acceleration
            //time to stop accelerating = initial velocity/gravity
            //Distance = acceleration / 2
            //Distance * 2 = Acceleration
            currentRigidBody.AddForce(Vector3.up * (m_characterHeight * m_jumpMultiplier) / m_jumpRate, ForceMode.VelocityChange);
            m_jumpState = JumpState.j_rising;
            StartCoroutine(JumpTransition());
        }

    }

    private IEnumerator JumpTransition()
    {
        while (m_jumpState != JumpState.j_grounded)
        {
            m_jumpProgress += Time.deltaTime / m_jumpRate;
            ProcessJump();
            yield return null;
        }
        ProcessJump();
    }

    public void ChangeActiveSkeletonMode(BodyState _state)
    {
        if (m_bodyState == _state)
            return;

        m_bodyState = _state;

        switch (_state)
        {
            case BodyState.b_skull:

                skullGameObject.SetActive(true);
                armsTorsoGameObject.SetActive(false);
                fullBodyGameObject.SetActive(false);

                skullGameObject.transform.position = currentActiveObject.transform.position;

                currentActiveObject = skullGameObject;

                camControl.orbitObject = currentActiveObject.transform;
                camControl.m_cameraOffset = 2;

                m_currentCollider = skullGameObject.GetComponent<Collider>();
                currentRigidBody = skullGameObject.GetComponent<Rigidbody>();
                animController?.ChangeAnimator(0);
                break;
            case BodyState.b_skullChest:
                skullGameObject.SetActive(false);
                armsTorsoGameObject.SetActive(true);
                fullBodyGameObject.SetActive(false);

                armsTorsoGameObject.transform.position = currentActiveObject.transform.position;

                currentActiveObject = armsTorsoGameObject;

                camControl.orbitObject = currentActiveObject.transform;
                camControl.m_cameraOffset = 3;

                m_currentCollider = armsTorsoGameObject.GetComponent<Collider>();
                currentRigidBody = armsTorsoGameObject.GetComponent<Rigidbody>();
                animController?.ChangeAnimator(1);

                currentMovementMode = MovementState.m_armRunning;
                currentRigidBody.AddForce(Vector3.up * skullOrientationBounce, ForceMode.VelocityChange);
                currentActiveObject.transform.rotation = Quaternion.LookRotation(mainCamera.transform.forward, Vector3.up);
                currentActiveObject.transform.eulerAngles = new Vector3(0, currentActiveObject.transform.eulerAngles.y, 0);
                currentRigidBody.constraints = RigidbodyConstraints.FreezeRotation;
                break;
            case BodyState.b_skullChestLegs:
                skullGameObject.SetActive(false);
                armsTorsoGameObject.SetActive(false);
                fullBodyGameObject.SetActive(true);

                fullBodyGameObject.transform.position = currentActiveObject.transform.position;

                currentActiveObject = fullBodyGameObject;
                camControl.orbitObject = currentActiveObject.transform;
                camControl.m_cameraOffset = 3;

                m_currentCollider = fullBodyGameObject.GetComponent<Collider>();
                currentRigidBody = fullBodyGameObject.GetComponent<Rigidbody>();
                animController?.ChangeAnimator(2);

                currentMovementMode = MovementState.m_running;
                currentRigidBody.AddForce(Vector3.up * skullOrientationBounce, ForceMode.VelocityChange);
                currentActiveObject.transform.rotation = Quaternion.LookRotation(mainCamera.transform.forward, Vector3.up);
                currentActiveObject.transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
                currentRigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                break;
            default:
                break;
        }
    }
}
