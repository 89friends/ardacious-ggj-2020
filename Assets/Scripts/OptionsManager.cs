﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    public static OptionsManager instance
    {
        get
        {
            return m_instance;
        }
    }
    private static OptionsManager m_instance = null;

    [SerializeField]
    private GameOptionsScriptableObject m_options;
    public GameOptionsScriptableObject options
    {
        get
        {
            return m_options;
        }
    }

    [Header("References")]
    [SerializeField]
    private Slider m_xSensitivitySlider;
    [SerializeField]
    private Slider m_ySensitivitySlider;
    [SerializeField]
    private Toggle m_subtitleToggle;
    [SerializeField]
    private Toggle m_yInvertToggle;

    private void Awake()
    {
        if (m_instance != null && m_instance != this)
        {
            Destroy(this.gameObject);
        }

        m_instance = this;
    }

    private void Start()
    {
        m_xSensitivitySlider.onValueChanged.AddListener(delegate { m_options.xAxisCameraSensitivity = m_xSensitivitySlider.value; });
        m_ySensitivitySlider.onValueChanged.AddListener(delegate { m_options.yAxisCameraSensitivity = m_ySensitivitySlider.value; });
        m_subtitleToggle.onValueChanged.AddListener(delegate { m_options.subtitles = m_subtitleToggle.isOn; });
        m_yInvertToggle.onValueChanged.AddListener(delegate { m_options.yAxisInvert = m_yInvertToggle.isOn; });
    }

    public void UpdateOptionValues()
    {
        m_xSensitivitySlider.value = m_options.xAxisCameraSensitivity;
        m_ySensitivitySlider.value = m_options.yAxisCameraSensitivity;
        m_subtitleToggle.isOn = m_options.subtitles;
        m_yInvertToggle.isOn = m_options.yAxisInvert;
    }
}
