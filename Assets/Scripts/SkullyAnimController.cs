﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimControllerState
{
    #region Just the Head
    e_HeadIdle,
    e_HeadMovement,
    #endregion

    #region Arms and Torso
    e_ArmIdle,
    e_ArmMovement,

    //climbing
    e_ArmClimbIdle,
    e_ArmClimbMovement,
    e_ArmMountingLadder,
    e_ArmDismountingLadder,

    e_HalfFallingInAir,
    #endregion

    #region Full Skele Man
    e_FullIdle,
    e_FullMovement,

    //jumping
    e_BeginJump,
    e_RisingInAir,
    e_FullFallingInAir,
    e_Landing,

    //climbing
    e_FullClimbIdle,
    e_FullClimbMovement,
    e_FullMountingLadder,
    e_FullDismountingLadder,
    #endregion
}

public class SkullyAnimController : MonoBehaviour
{
    [SerializeField]
    private Animator skeletonHeadAnimator, skeletonArmsAnimator, skeletonFullAnimator;

    private Animator m_currentActiveAnimator;

    public AnimControllerState m_currentAnimationState = AnimControllerState.e_HeadIdle;

    private void Start()
    {
        ChangeAnimator(0);
    }

    private void ProcessAnimState()
    {
        string stateNameToPlay = string.Empty;

        if (m_currentActiveAnimator == null)
            return;

        switch (m_currentAnimationState)
        {
            case AnimControllerState.e_HeadIdle:
                stateNameToPlay = "GGJ2020_Char_SkeletonSkull_Idle_01";
                break;
            case AnimControllerState.e_ArmIdle:
                stateNameToPlay = "SkeletonHalf_Idle";
                break;
            case AnimControllerState.e_ArmMovement:
                stateNameToPlay = "SkeletonHalf_Run";
                break;
            case AnimControllerState.e_ArmClimbMovement:
                stateNameToPlay = "SkeletonHalf_ClimbLoop";
                break;
            case AnimControllerState.e_FullIdle:
                stateNameToPlay = "SkeletonFull_Idle";
                break;
            case AnimControllerState.e_FullMovement:
                stateNameToPlay = "SkeletonFull_Run";
                break;
            case AnimControllerState.e_BeginJump:
                stateNameToPlay = "SkeletonFull_JumpUp";
                break;
            case AnimControllerState.e_RisingInAir:
                stateNameToPlay = "SkeletonFull_JumpLoop";
                break;
            case AnimControllerState.e_FullFallingInAir:
                stateNameToPlay = "SkeletonFull_JumpLoop";
                break;
            case AnimControllerState.e_HalfFallingInAir:
                stateNameToPlay = "SkeletonHalf_JumpLoop";
                break;
            case AnimControllerState.e_Landing:
                stateNameToPlay = "SkeletonFull_JumpEnd";
                break;
            case AnimControllerState.e_FullClimbMovement:
                stateNameToPlay = "SkeletonFull_ClimbLoop";
                break;
        }

        if (string.IsNullOrEmpty(stateNameToPlay))
        {
            //play full idle as default
        }

        m_currentActiveAnimator?.Play(stateNameToPlay);
    }

    public void ChangeAnimState(AnimControllerState _newState)
    {
        m_currentAnimationState = _newState;
        ProcessAnimState();
    }

    public void SetPlayerMoving(bool _isMoving)
    {
        skeletonFullAnimator.SetBool("PlayerMoving", _isMoving);
    }

    public AnimControllerState CurrentAnimationState()
    {
        return m_currentAnimationState;
    }

    public void ChangeAnimator(int _animIndex)
    {
        switch (_animIndex)
        {
            default:
            case 0:
                m_currentActiveAnimator = skeletonHeadAnimator;
                break;
            case 1:
                m_currentActiveAnimator = skeletonArmsAnimator;
                break;
            case 2:
                m_currentActiveAnimator = skeletonFullAnimator;
                break;
        }
    }
}
