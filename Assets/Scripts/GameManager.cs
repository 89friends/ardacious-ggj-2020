﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance
    {
        get
        {
            return m_instance;
        }
    }
    private static GameManager m_instance = null;

    [SerializeField]
    private GameObject m_pauseMenu;
    [SerializeField]
    private GameObject m_optionsMenu;

    private SceneTransition m_sceneTransition;

    public bool isPaused
    {
        get
        {
            return m_isPaused;
        }
    }
    private bool m_isPaused;

    private void Awake()
    {
        if (m_instance != null && m_instance != this)
        {
            Destroy(this.gameObject);
        }

        m_instance = this;

        if (m_pauseMenu == null)
        {
            Debug.LogError("GameManager: Pause Menu has not been assigned in the inspector!");
        }

        if (m_optionsMenu == null)
        {
            Debug.LogError("GameManager: Options Menu has not been assigned in the inspector!");
        }

        m_sceneTransition = GetComponent<SceneTransition>();
        if (m_sceneTransition == null)
        {
            Debug.LogError("GameManager: No SceneTransition found on GameManager!");
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePaused();
        }

        if (Input.GetKeyDown(KeyCode.Equals))
        {
            RestartLevel();
        }
    }

    public void OnResumePressed()
    {
        TogglePaused();
    }

    public void OnRestartPressed()
    {
        RestartLevel();
    }

    public void OnOptionsPressed()
    {
        OptionsManager.instance.UpdateOptionValues();
        m_optionsMenu.SetActive(true);
    }

    public void OnExitPressed()
    {
        TogglePaused(true);
        StartCoroutine(m_sceneTransition.ExitGameWithTransition());
    }

    /// <summary>
    /// Toggles whether the game is paused.
    /// </summary>
    /// <param name="triggeringTransition">In the case that the scene is restarting, we don't want to let anything start moving, etc.</param>
    private void TogglePaused(bool triggeringTransition = false)
    {
        Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        m_pauseMenu.SetActive(!m_pauseMenu.activeSelf);

        if (!triggeringTransition)
        {
            m_isPaused = !m_isPaused;
        }
    }

    private void RestartLevel()
    {
        TogglePaused(true);
        StartCoroutine(m_sceneTransition.ReloadSceneWithTransition());
    }

    public void CloseOptionsMenu()
    {
        m_optionsMenu.SetActive(false);
    }
}
