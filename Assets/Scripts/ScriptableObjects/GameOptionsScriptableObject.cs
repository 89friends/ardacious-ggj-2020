﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameOptions", menuName = "ScriptableObjects/GameOptions", order = 1)]
public class GameOptionsScriptableObject : ScriptableObject
{
    [Header("Camera Options")]
    public float xAxisCameraSensitivity;
    public float yAxisCameraSensitivity;
    public bool yAxisInvert;

    [Space(10)]
    public bool subtitles;
}