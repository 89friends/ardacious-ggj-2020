﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    [SerializeField]
    private Animator m_transitionAnim;

    public IEnumerator LoadCreditsWithTransition()
    {
        m_transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(1);
    }

    public IEnumerator ReloadSceneWithTransition()
    {
        m_transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public IEnumerator ExitGameWithTransition()
    {
        m_transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(1.5f);
        Debug.Log("GameManager: Application.Quit() called here.");
        Application.Quit();
    }
}
