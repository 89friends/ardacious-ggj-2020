﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CreditsController : MonoBehaviour
{
    private bool audioCredits = false;

    [SerializeField, TextArea]
    private string credits;
    [SerializeField, TextArea]
    private string audioCreditsText;

    [SerializeField]
    private Animator m_fadeInAnim;

    [SerializeField]
    private TextMeshProUGUI text;

    [SerializeField]
    private TextMeshProUGUI swapButton;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void OnRestartPressed()
    {
        StartCoroutine(FadeOutLoadScene());
    }

    private IEnumerator FadeOutLoadScene()
    {
        m_fadeInAnim.SetTrigger("end");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(0);
    }

    public void SwapCredits()
    {
        if (audioCredits)
        {
            text.text = credits;
            swapButton.text = "Audio Credits";
        }
        else
        {
            text.text = audioCreditsText;
            swapButton.text = "Team Credits";
        }

        audioCredits = !audioCredits;
    }
}
