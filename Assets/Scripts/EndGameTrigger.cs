﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameTrigger : MonoBehaviour
{
    [SerializeField]
    private Camera mainCamera;

    [SerializeField]
    private Camera cutsceneCamera;

    [SerializeField]
    private Animator skellyAnimator;

    [SerializeField]
    private Animator ripAnimator;

    [SerializeField]
    private SceneTransition transition;

    private bool triggered;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("EndGameTrigger: Player has hit end game.");
        triggered = true;

        mainCamera.gameObject.SetActive(false);
        cutsceneCamera.gameObject.SetActive(true);
        StartCoroutine(EndGameJump());
    }

    private IEnumerator EndGameJump()
    {
        yield return new WaitForSeconds(2.0f);
        skellyAnimator.SetTrigger("endgame");
        yield return new WaitForSeconds(1.5f);
        ripAnimator.SetTrigger("endgame");
        yield return new WaitForSeconds(3.0f);
        StartCoroutine(transition.LoadCreditsWithTransition());
    }
}
