﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour
{
    [SerializeField]
    private AudioManager m_manager;
    [SerializeField]
    private AudioClip m_audioClipToPlay;

    private bool triggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!triggered)
        {
            m_manager.PlayNextTrack(m_audioClipToPlay);
            triggered = true;
        }
    }
}
