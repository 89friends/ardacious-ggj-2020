﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubtitleTrigger : MonoBehaviour
{
    [SerializeField]
    private List<Subtitle> m_subtitles = new List<Subtitle>();
    [SerializeField]
    private Text m_subtitleObject;

    private bool triggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!triggered && OptionsManager.instance.options.subtitles)
        {
            StartCoroutine(PlaySubtitles());
            triggered = true;
        }
    }

    private IEnumerator PlaySubtitles()
    {
        foreach (Subtitle subtitle in m_subtitles)
        {
            m_subtitleObject.text = subtitle.text;
            yield return new WaitForSeconds(subtitle.secsToChange);
        }

        m_subtitleObject.text = "";
    }
}

[System.Serializable]
public class Subtitle
{
    public string text;
    public float secsToChange;
}
