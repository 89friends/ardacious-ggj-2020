﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private bool fadeTracks = true; // Should be false when we use this for dialogue.

    private AudioSource m_audioSource;
    private float m_startVolume;

    private void Start()
    {
        m_audioSource = GetComponent<AudioSource>();
        m_startVolume = m_audioSource.volume;
    }

    public void PlayNextTrack(AudioClip _audioClip)
    {
        if (m_audioSource.clip != null && fadeTracks)
        {
            StartCoroutine(FadeOutToNextTrack(1.0f, _audioClip));
        }
        else
        {
            m_audioSource.clip = _audioClip;
            m_audioSource.Play();
        }
    }

    public IEnumerator FadeOutToNextTrack(float _fadeTime, AudioClip _audioClip)
    {
        while (m_audioSource.volume > 0)
        {
            m_audioSource.volume -= m_startVolume * Time.deltaTime / _fadeTime;
            yield return null;
        }

        m_audioSource.Stop();
        m_audioSource.clip = _audioClip;
        m_audioSource.Play();

        while (m_audioSource.volume < m_startVolume)
        {
            m_audioSource.volume += m_startVolume * Time.deltaTime / _fadeTime;
            yield return null;
        }
    }
}
