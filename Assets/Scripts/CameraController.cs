﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("Y-Axis Clamping")]
    [SerializeField]
    private float m_yAxisMax = 5.0f;
    [SerializeField]
    private float m_yAxisMin = -5.0f;

    [Space(10)]
    public Transform orbitObject;

    [SerializeField]
    public float m_cameraOffset = 10.0f;
    private float m_currentX = 0.0f;
    private float m_currentY = 0.0f;

    [SerializeField]
    private KeyCode m_cursorToggleInput;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        if (!GameManager.instance.isPaused)
        {
            m_currentX += Input.GetAxis("Mouse X") * OptionsManager.instance.options.xAxisCameraSensitivity;

            if (OptionsManager.instance.options.yAxisInvert)
            {
                m_currentY += Input.GetAxis("Mouse Y") * -OptionsManager.instance.options.yAxisCameraSensitivity;
            }
            else
            {
                m_currentY += Input.GetAxis("Mouse Y") * OptionsManager.instance.options.yAxisCameraSensitivity;
            }

            m_currentY = Mathf.Clamp(m_currentY, m_yAxisMin, m_yAxisMax);
        }

        if (Input.GetKeyDown(m_cursorToggleInput))
            ToggleCursorLockState();
    }

    private void LateUpdate()
    {
        if (!GameManager.instance.isPaused)
        {
            Vector3 dir = new Vector3(0, 0, -m_cameraOffset);
            Quaternion rotation = Quaternion.Euler(m_currentY, m_currentX, 0);
            transform.position = orbitObject.position + rotation * dir;
            transform.LookAt(orbitObject.position);
        }
    }

    private void ToggleCursorLockState ()
    {
        if (Cursor.lockState != CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
