﻿using UnityEngine;

public class CollisionLogic : MonoBehaviour
{
    public SkullyMovementController movementController;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ChestPickup"))
        {
            movementController.ChangeActiveSkeletonMode(SkullyMovementController.BodyState.b_skullChest);
            other.gameObject.SetActive(false);
        }

        if (other.CompareTag("LegsPickup") && movementController.m_bodyState == SkullyMovementController.BodyState.b_skullChest)
        {
            movementController.ChangeActiveSkeletonMode(SkullyMovementController.BodyState.b_skullChestLegs);
            other.gameObject.SetActive(false);
        }

        if (other.CompareTag("Ladder") && movementController.m_bodyState != SkullyMovementController.BodyState.b_skull)
        {
            movementController.ChangeMovementState(SkullyMovementController.MovementState.m_climbing);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ladder"))
        {
            movementController.ChangeMovementState(movementController.currentMovementMode);
            movementController.currentRigidBody.AddForce(Vector3.up * movementController.skullOrientationBounce, ForceMode.VelocityChange);
        }
    }
}
